//
//  ViewController.swift
//  fridaTestApp
//
//  Created by Korakod Saraboon on 27/9/2566 BE.
//

import UIKit

class TestViewController: UIViewController {
  
  @IBOutlet weak var label: UILabel!
  var d = 1
  @IBOutlet weak var button: UIButton!
  var labelText: String = "1199-9232-9494"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    savePassword()
    label.text = labelText
  }
  
  @objc func savePassword() {
    UserDefaults.standard.setValue("1122334455", forKey: "savePassword")
    UserDefaults.standard.setValue("klakub", forKey: "saveUsername")
    UserDefaults.standard.setValue("keyja", forKey: "privateKey")
  }
  
  @IBAction func buttonRemoveAction(_ sender: Any) {
    NSLog("Try to remove censor text")
    // function call
    label.text = labelText;
  }
  
  @IBAction func buttonAssignAction(_ sender: Any) {
    assignStarToLabel()
  }
  
  @objc func returnCensorText(text: String) -> String {
    return text.replacing(last: 4, with: "*")
  }
  
  @objc func assignStarToLabel() {
    label.text = returnCensorText(text: label.text ?? "")
  }

}

extension String {
  func replacing(last n: Int, with s: String) -> String {
        let replacement = String(repeating: s, count: min(count, n))
        return dropLast(n) + replacement
    }
}

